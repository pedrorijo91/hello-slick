package com.example

import scala.slick.driver.H2Driver
import scala.slick.driver.H2Driver.simple._

object Hello {

  // Definition of the SUPPLIERS table
  class Suppliers(tag: Tag) extends Table[(Int, String, String, String, String, String)](tag, "SUPPLIERS") {
    def id = column[Int]("SUP_ID", O.PrimaryKey) // This is the primary key column
    def name = column[String]("SUP_NAME")
    def street = column[String]("STREET")
    def city = column[String]("CITY")
    def state = column[String]("STATE")
    def zip = column[String]("ZIP")
    // Every table needs a * projection with the same type as the table's type parameter
    def * = (id, name, street, city, state, zip)
  }
  val suppliers = TableQuery[Suppliers]

  // Definition of the COFFEES table
  class Coffees(tag: Tag) extends Table[(String, Int, Double, Int, Int)](tag, "COFFEES") {
    def name = column[String]("COF_NAME", O.PrimaryKey)
    def supID = column[Int]("SUP_ID")
    def price = column[Double]("PRICE")
    def sales = column[Int]("SALES")
    def total = column[Int]("TOTAL")
    def * = (name, supID, price, sales, total)
    // A reified foreign key relation that can be navigated to create a join
    def supplier = foreignKey("SUP_FK", supID, suppliers)(_.id)
  }
  val coffees = TableQuery[Coffees]

  val db = Database.forURL("jdbc:h2:mem:test1", driver = "org.h2.Driver")

  def populateDB()(implicit session: H2Driver.backend.Session) = {

    // Create the tables, including primary and foreign keys
    (suppliers.ddl ++ coffees.ddl).create

    // Insert some suppliers
    suppliers += (101, "Acme, Inc.",      "99 Market Street", "Groundsville", "CA", "95199")
    suppliers += ( 49, "Superior Coffee", "1 Party Place",    "Mendocino",    "CA", "95460")
    suppliers += (150, "The High Ground", "100 Coffee Lane",  "Meadows",      "CA", "93966")

    // Insert some coffees (using JDBC's batch insert feature, if supported by the DB)
    coffees ++= Seq(
      ("Colombian",         101, 7.99, 0, 0),
      ("French_Roast",       49, 8.99, 0, 0),
      ("Espresso",          150, 9.99, 0, 0),
      ("Colombian_Decaf",   101, 8.99, 0, 0),
      ("French_Roast_Decaf", 49, 9.99, 0, 0)
    )
  }

  def s0()(implicit session: H2Driver.backend.Session) = {
    println("// Iterate through all coffees and output them")
    coffees foreach {
      case (name, supID, price, sales, total) => println("  " + name + "\t" + supID + "\t" + price + "\t" + sales + "\t" + total)
    }
  }

  def s1()(implicit session: H2Driver.backend.Session) = {
    println("// Why not let the database do the string conversion and concatenation?")
    val q1 = for(c <- coffees)
    yield LiteralColumn("  ") ++ c.name ++ "\t" ++ c.supID.asColumnOf[String] ++
        "\t" ++ c.price.asColumnOf[String] ++ "\t" ++ c.sales.asColumnOf[String] ++
        "\t" ++ c.total.asColumnOf[String]
    println("// The first string constant needs to be lifted manually to a LiteralColumn so that the proper ++ operator is found")
    q1 foreach println
  }

  def s2()(implicit session: H2Driver.backend.Session) = {
    println("// Perform a join to retrieve coffee names and supplier names for all coffees costing less than $9.00")
    val q2 = for {
      c <- coffees if c.price < 9.0
      s <- suppliers if s.id === c.supID
    } yield (c.name, s.name)
    q2 foreach println
  }

  def s3()(implicit session: H2Driver.backend.Session) = {
    println("// q3")
    val q3 = for {
      c <- coffees if c.price < 9.0
      s <- c.supplier
    } yield (c.name, s.name)
    q3 foreach println
  }

  def main(args: Array[String]): Unit = {

    db withSession {
      implicit session => {
        populateDB()

        s0()
        s1()
        s2()
        s3()
      }
    }
  }


}
